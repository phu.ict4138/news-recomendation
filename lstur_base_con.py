import torch
import torch.nn as nn
import numpy as np
from config import *
from layer import AdditiveAttention
from dataset import Data
from torch.utils.data import DataLoader


class LSTURBaseCon(nn.Module):
    def __init__(self):
        super(LSTURBaseCon, self).__init__()

        self.news_encoder = NewsEncoder()
        self.user_encoder = UserEncoder()

    def forward(
        self,
        user,
        history_category,
        history_subcategory,
        history_title,
        candidate_category,
        candidate_subcategory,
        candidate_title,
    ):
        history = torch.concatenate([
            history_category.unsqueeze(-1),
            history_subcategory.unsqueeze(-1),
            history_title
        ], dim=-1)  # shape: bz * number_history*(1+1+30+50) * !
        candidate = torch.concatenate([
            candidate_category.unsqueeze(-1),
            candidate_subcategory.unsqueeze(-1),
            candidate_title
        ], dim=-1)  # shape: bz * number_candidate*(1+1+30+50)
        candidate_present = self.news_encoder(candidate)
        history_encoder = self.news_encoder(history)
        user_encode = self.user_encoder(user, history_encoder) # bz * 1 * 1200
        output = torch.bmm(
            candidate_present,
            user_encode.unsqueeze(-1)
        ).squeeze(-1)  # bz*5
        return output


class NewsEncoder(nn.Module):
    def __init__(self):
        super(NewsEncoder, self).__init__()

        embedding_path = "./data/util/word_embedding.npy"
        embedding_weight = torch.from_numpy(np.load(embedding_path)).float()
        self.word_embedding = nn.Embedding.from_pretrained(
            embedding_weight,
            freeze=False
        )

        self.category_embedding = nn.Embedding(
            num_embeddings=NUMBER_CATEGORY,
            embedding_dim=CATEGORY_DIM
        )
        # self.category_linear = nn.Linear(
        #     in_features=CATEGORY_DIM,
        #     out_features=NUMBER_FILTER_CNN
        # )

        self.subcategory_embedding = nn.Embedding(
            num_embeddings=NUMBER_SUBCATEGORY,
            embedding_dim=SUBCATEGORY_DIM
        )
        # self.subcategory_linear = nn.Linear(
        #     in_features=SUBCATEGORY_DIM,
        #     out_features=NUMBER_FILTER_CNN
        # )

        self.title_cnn = nn.Conv2d(
            in_channels=1,
            out_channels=NUMBER_FILTER_CNN,
            kernel_size=(WINDOW_SIZE, WORD_EMBEDDING_DIM),
            padding=(1, 0)
        )

        self.title_relu = nn.ReLU()
        self.title_drop = nn.Dropout(p=DROPOUT)

        self.attention_title = AdditiveAttention(
            d_h=NUMBER_FILTER_CNN,
            hidden_size=QUERRY_DIM
        )

    def forward(self, x):
        bz = x.shape[0]  # batch size
        num = x.shape[1]  # number of news
        _x = x.flatten(0, 1)  # flat*(1+1+30) #flat = bz * num

        category = _x[:, :1]  # flat*1
        subcategory = _x[:, 1:2]  # flat*1
        title = _x[:, 2:]  # flat*30

        category_emb = self.category_embedding(category)  # flat*1*100
        # category_linear = self.category_linear(category_emb)  # flat*1*400

        subcategory_emb = self.subcategory_embedding(subcategory)  # flat*1*100
        # subcategory_linear = self.subcategory_linear(
        #     subcategory_emb
        # )  # flat*1*400
        title_emb = self.word_embedding(title)
        title_drop = self.title_drop(title_emb)  # flat*30*300
        title_drop = title_drop.unsqueeze(1)  # flat*1*30*300
        title_cnn = self.title_cnn(title_drop)
        title_relu = self.title_relu(title_cnn)  # flat*400*30*1
        title_relu = title_relu.squeeze(-1).transpose(2, 1)  # flat*30*400
        title_att = self.attention_title(title_relu)  # flat * 400
        concate = torch.concatenate([
            category_emb.squeeze(1),
            subcategory_emb.squeeze(1),
            title_att
        ], dim=1)
        out = concate.reshape(bz, num, 600)
        return out


class UserEncoder(nn.Module):
    def __init__(self):
        super(UserEncoder, self).__init__()

        self.gru = nn.GRU(
            600,
            300,
            batch_first=True
        )

        self.user_embedding = nn.Embedding(
            NUMBER_USER,
            300
        )

        self.user_marking = nn.Dropout(p=0.5)

    def forward(self, user, history):
        """
            user: bz * 1
            history: bz * num * 600
        """
        output, hn = self.gru(history) 
        user_emb = self.user_embedding(user) 
        user_dropt = self.user_marking(user_emb)  # bz * 1* 600

        out = torch.concatenate([
            hn.squeeze(0),
            user_dropt
        ], dim=1)
        return out


if __name__ == "__main__":
    train = Data(mode="train")
    dataload = DataLoader(train, batch_size=32)
    it = iter(dataload)
    batch = next(it)
    model = LSTURBaseCon()
    x = model(
        batch["user"],
        batch["history_category"],
        batch["history_subcategory"],
        batch["history_title"],
        batch["candidate_category"],
        batch["candidate_subcategory"],
        batch["candidate_title"],
    )
    print(x.shape)
