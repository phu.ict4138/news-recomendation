from torch.utils.data import Dataset, DataLoader
import json
import pandas as pd
from tqdm import tqdm

from util import *
from config import *


class Data(Dataset):
    def __init__(self, mode="train"):
        """
            mode: train | val | test
        """
        super(Data, self).__init__()
        self.mode = mode

        print("Loading dictionary...")
        self.word_dict = load_dict("./data/util/word2index.csv")
        self.category_dict = load_dict("./data/util/category2index.csv")
        self.subcategory_dict = load_dict("./data/util/subcategory2index.csv")
        self.entity_dict = load_dict("./data/util/entity2index.csv")
        self.user_dict = load_dict("./data/util/user2index.csv")
        print("Load dictionary done!")
        self.news_dict = {}
        self.impress = []
        self.load_news()
        self.load_behavior()

    def load_news(self):
        self.news_dict[0] = {
            "category": 0,
            "subcategory": 0,
            "title": [0] * TITLE_SIZE,
            "abstract": [0] * ABSTRACT_SIZE,
            "titleentity": [0] * TITLE_ENTITY_SIZE,
            "abstractentity": [0] * ABSTRACT_ENTITY_SIZE,
        }
        df = pd.read_csv(f"./data/{self.mode}/news.tsv", sep="\t", header=None)
        df.columns = ['id', "category", "subcategory", "title",
                      "abstract", "url", "titleentity", "abstractentity"]
        df["titleentity"].fillna("[]", inplace=True)
        df["abstractentity"].fillna("[]", inplace=True)
        df["title"].fillna("", inplace=True)
        df["abstract"].fillna("", inplace=True)
        df.fillna(" ", inplace=True)
        new_id_ = df["id"].tolist()
        category_ = df["category"].tolist()
        subcategory_ = df["subcategory"].tolist()
        title_ = df["title"].tolist()
        abstract_ = df["abstract"].tolist()
        titleentity_ = df["titleentity"].tolist()
        abstractentity_ = df["abstractentity"].tolist()
        titleentity_ = [json.loads(i) for i in titleentity_]
        abstractentity_ = [json.loads(i) for i in abstractentity_]
        n = df.__len__()
        for i in tqdm(range(n), desc="Reading news..."):
            new_id = new_id_[i]
            category = category_[i]
            subcategory = subcategory_[i]
            title = title_[i]
            abstract = abstract_[i]
            title_entity = titleentity_[i]
            abstract_entity = abstractentity_[i]
            if category in self.category_dict:
                category = self.category_dict[category]
            else:
                category = 0
            if subcategory in self.subcategory_dict:
                subcategory = self.subcategory_dict[subcategory]
            else:
                subcategory = 0
            _title = word_tokenize(title)
            if len(_title) > TITLE_SIZE:
                _title = title_[:TITLE_SIZE]
                for _ in range(TITLE_SIZE):
                    if _title[_] in self.word_dict:
                        _title[_] = self.word_dict(_title[_])
                    else:
                        _title[_] = 0
            else:
                for _ in range(len(_title)):
                    if _title[_] in self.word_dict:
                        _title[_] = self.word_dict[_title[_]]
                    else:
                        _title[_] = 0
                _title = _title + [0] * (TITLE_SIZE - len(_title))
            _abstract = word_tokenize(abstract)
            if len(_abstract) > ABSTRACT_SIZE:
                _abstract = abstract_[:ABSTRACT_SIZE]
                for _ in range(ABSTRACT_SIZE):
                    if _abstract[_] in self.word_dict:
                        _abstract[_] = self.word_dict[_abstract[_]]
                    else:
                        _abstract[_] = 0
            else:
                for _ in range(len(_abstract)):
                    if _abstract[_] in self.word_dict:
                        _abstract[_] = self.word_dict[_abstract[_]]
                    else:
                        _abstract[_] = 0
                _abstract = _abstract + [0] * (ABSTRACT_SIZE - len(_abstract))

            _title_entity = [i["WikidataId"] for i in title_entity]
            for _ in range(len(_title_entity)):
                if _title_entity[_] in self.entity_dict:
                    _title_entity[_] = self.entity_dict[_title_entity[_]]
                else:
                    _title_entity[_] = 0
            if len(_title_entity) > TITLE_ENTITY_SIZE:
                _title_entity = _title_entity[:TITLE_ENTITY_SIZE]
            else:
                _title_entity = _title_entity + \
                    [0] * (TITLE_ENTITY_SIZE - len(_title_entity))

            _abstract_entity = [i["WikidataId"] for i in abstract_entity]
            for _ in range(len(_abstract_entity)):
                if _abstract_entity[_] in self.entity_dict:
                    _abstract_entity[_] = self.entity_dict[_abstract_entity[_]]
                else:
                    _abstract_entity[_] = 0
            if len(_abstract_entity) > ABSTRACT_ENTITY_SIZE:
                _abstract_entity = _abstract_entity[:ABSTRACT_ENTITY_SIZE]
            else:
                _abstract_entity = _abstract_entity + \
                    [0] * (ABSTRACT_ENTITY_SIZE - len(_abstract_entity))

            self.news_dict[new_id] = {
                "category": category,
                "subcategory": subcategory,
                "title": _title,
                "abstract": _abstract,
                "titleentity": _title_entity,
                "abstractentity": _abstract_entity,
            }

    def load_behavior(self):
        df = pd.read_csv(
            f"./data/{self.mode}/behaviors.tsv", sep="\t", header=None)
        df.columns = ['id', "userId", "time", "history", "click"]
        df.fillna("", inplace=True)
        _user = df["userId"].tolist()
        _history = df["history"].tolist()
        _click = df["click"].tolist()
        n = df.__len__()
        for i in tqdm(range(n), desc="Parcing behavior..."):
            user = _user[i]
            history = _history[i]
            click = _click[i]
            if user in self.user_dict:
                user = self.user_dict[user]
            else:
                user = 0
            if history == "":
                history = []
            else:
                history = history.split(" ")
            if len(history) > HISTORY_SIZE:
                history = history[:HISTORY_SIZE]
            else:
                history = [0] * (HISTORY_SIZE - len(history)) + history
            if self.mode == "test":
                candidate = click.split(" ")
                label = [0]*len(candidate)
                self.impress.append({
                    "user": user,
                    "history": history,
                    "candidate": candidate,
                    "label": label,
                })
            elif self.mode == "val":
                __click = click.split(" ")
                candidate = [i.split("-")[0] for i in __click]
                label = [int(i.split("-")[1]) for i in __click]
                self.impress.append({
                    "user": user,
                    "history": history,
                    "candidate": candidate,
                    "label": label,
                })
            else:
                click = click.split(" ")
                positive = []
                negative = []
                for _ in range(len(click)):
                    new, lb = click[_].split("-")
                    if lb == "1":
                        positive.append(new)
                    else:
                        negative.append(new)
                for _ in positive:
                    candidate = [_] + new_sample(negative, NPRATIO)
                    self.impress.append({
                        "user": user,
                        "history": history,
                        "candidate": candidate,
                        "label": [1] + [0]*NPRATIO
                    })
        return

    def __len__(self):
        return len(self.impress)

    def __getitem__(self, index):
        impress = self.impress[index]
        user = impress["user"]
        history = impress["history"]
        candidate = impress["candidate"]
        label = impress["label"]
        history = [self.news_dict[i] for i in history]
        candidate = [self.news_dict[i] for i in candidate]
        item = {
            "user": torch.tensor(user),
            "history_category": torch.stack([torch.tensor(i["category"]) for i in history]),
            "history_subcategory": torch.stack([torch.tensor(i["subcategory"]) for i in history]),
            "history_title": torch.stack([torch.tensor(i["title"]) for i in history]),
            "history_abstract": torch.stack([torch.tensor(i["abstract"]) for i in history]),
            "history_title_entity": torch.stack([torch.tensor(i["titleentity"]) for i in history]),
            "history_abstract_entity": torch.stack([torch.tensor(i["abstractentity"]) for i in history]),
            "candidate_category": torch.stack([torch.tensor(i["category"]) for i in candidate]),
            "candidate_subcategory": torch.stack([torch.tensor(i["subcategory"]) for i in candidate]),
            "candidate_title": torch.stack([torch.tensor(i["title"]) for i in candidate]),
            "candidate_abstract": torch.stack([torch.tensor(i["abstract"]) for i in candidate]),
            "candidate_title_entity": torch.stack([torch.tensor(i["titleentity"]) for i in candidate]),
            "candidate_abstract_entity": torch.stack([torch.tensor(i["abstractentity"]) for i in candidate]),
            "label": torch.tensor(label).float()
        }
        return item


if __name__ == "__main__":
    data = Data(mode="train")
    dataload = DataLoader(data, batch_size=32)
    it = iter(dataload)
    batch = next(it)
    print("history_category", batch["history_category"].shape)
    print("history_title", batch["history_title"].shape)
    print("history_title_entity", batch["history_title_entity"].shape)
    print("candidate_category", batch["candidate_category"].shape)
    print("candidate_title", batch["candidate_title"].shape)
    print("candidate_abstract_entity",
          batch["candidate_abstract_entity"].shape)
