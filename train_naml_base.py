from dataset import Data
from torch.utils.data import DataLoader
from tqdm import tqdm
from naml_base import NAMLBase
import torch.nn as nn
import torch
from config import *
from util import ndcg_score, roc_auc_score, mrr_score


def evaluate(model):
    data = Data(mode="val")
    validload = DataLoader(data, batch_size=1, shuffle=False)
    ndcg5 = 0
    ndcg10 = 0
    auc = 0
    mrr = 0
    k = 0
    with torch.no_grad():
        for batch in tqdm(validload, desc="Validing..."):
            k += 1
            pred = model(
                batch["history_category"].to(DEVICE),
                batch["history_subcategory"].to(DEVICE),
                batch["history_title"].to(DEVICE),
                batch["history_abstract"].to(DEVICE),
                batch["candidate_category"].to(DEVICE),
                batch["candidate_subcategory"].to(DEVICE),
                batch["candidate_title"].to(DEVICE),
                batch["candidate_abstract"].to(DEVICE)
            ).squeeze(0).cpu().detach().numpy()
            label = batch["label"].squeeze(0).cpu().detach().numpy()
            _ndcg5 = ndcg_score(label, pred, k=5)
            _ndcg10 = ndcg_score(label, pred, k=10)
            _auc = roc_auc_score(label, pred)
            _mrr = mrr_score(label, pred)
            ndcg5 += _ndcg5
            ndcg10 += _ndcg10
            auc += _auc
            mrr += _mrr
            if k == 10000:
                break
    print(
        f"Evaluate done!: auc: {round(auc/k, 4)}, mrr: {round(mrr/k, 4)} nDCG@5: {round(ndcg5/k, 4)}, nDCG@10: {round(ndcg10/k, 4)}")


if __name__ == "__main__":
    model = NAMLBase()
    model.to(DEVICE)
    print(model)
    print(sum(i.numel() for i in model.parameters()))
    loss_fn = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=LEARNING_RATE)
    train = Data(mode="train")
    dataload = DataLoader(train, batch_size=BATCH_SIZE, shuffle=True)
    step = 0
    total_loss = 0
    for i in range(1):
        for batch in tqdm(dataload, desc="Training..."):
            step += 1
            pred = model(
                batch["history_category"].to(DEVICE),
                batch["history_subcategory"].to(DEVICE),
                batch["history_title"].to(DEVICE),
                batch["history_abstract"].to(DEVICE),
                batch["candidate_category"].to(DEVICE),
                batch["candidate_subcategory"].to(DEVICE),
                batch["candidate_title"].to(DEVICE),
                batch["candidate_abstract"].to(DEVICE)
            )
            label = batch["label"].to(DEVICE)
            loss = loss_fn(pred, label)
            total_loss += loss.item()
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if step % STEP_LOSS == 0:
                print("Loss:", total_loss / STEP_LOSS)
                total_loss = 0
            if step == 2000:
                break
    evaluate(model)
