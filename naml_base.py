import torch
import torch.nn as nn
import numpy as np
from config import *
from layer import AdditiveAttention
from dataset import Data
from torch.utils.data import DataLoader


class NAMLBase(nn.Module):
    def __init__(self):
        super(NAMLBase, self).__init__()

        self.news_encoder = NewsEncoder()
        self.user_encoder = UserEncoder()

    def forward(
        self,
        history_category,
        history_subcategory,
        history_title,
        history_abstract,
        candidate_category,
        candidate_subcategory,
        candidate_title,
        candidate_abstract
    ):
        history = torch.concatenate([
            history_category.unsqueeze(-1),
            history_subcategory.unsqueeze(-1),
            history_title,
            history_abstract
        ], dim=-1)  # shape: bz * number_history*(1+1+30+50) * !
        candidate = torch.concatenate([
            candidate_category.unsqueeze(-1),
            candidate_subcategory.unsqueeze(-1),
            candidate_title,
            candidate_abstract
        ], dim=-1)  # shape: bz * number_candidate*(1+1+30+50)
        history_encoder = self.news_encoder(history)
        user_present = self.user_encoder(history_encoder)
        candidate_present = self.news_encoder(candidate)
        output = torch.bmm(
            candidate_present,
            user_present.unsqueeze(-1)
        ).squeeze(-1)  # bz*5
        return output


class NewsEncoder(nn.Module):
    def __init__(self):
        super(NewsEncoder, self).__init__()

        embedding_path = "./data/util/word_embedding.npy"
        embedding_weight = torch.from_numpy(np.load(embedding_path)).float()
        self.word_embedding = nn.Embedding.from_pretrained(
            embedding_weight,
            freeze=False
        )

        self.category_embedding = nn.Embedding(
            num_embeddings=NUMBER_CATEGORY,
            embedding_dim=CATEGORY_DIM
        )
        self.category_linear = nn.Linear(
            in_features=CATEGORY_DIM,
            out_features=NUMBER_FILTER_CNN
        )

        self.subcategory_embedding = nn.Embedding(
            num_embeddings=NUMBER_SUBCATEGORY,
            embedding_dim=SUBCATEGORY_DIM
        )
        self.subcategory_linear = nn.Linear(
            in_features=SUBCATEGORY_DIM,
            out_features=NUMBER_FILTER_CNN
        )

        self.title_cnn = nn.Conv2d(
            in_channels=1,
            out_channels=NUMBER_FILTER_CNN,
            kernel_size=(WINDOW_SIZE, WORD_EMBEDDING_DIM),
            padding=(1, 0)
        )

        self.title_relu = nn.ReLU()
        self.title_drop = nn.Dropout(p=DROPOUT)

        self.abstract_cnn = nn.Conv2d(
            in_channels=1,
            out_channels=NUMBER_FILTER_CNN,
            kernel_size=(WINDOW_SIZE, WORD_EMBEDDING_DIM),
            padding=(1, 0)
        )

        self.abstract_relu = nn.ReLU()
        self.abstract_drop = nn.Dropout(p=DROPOUT)

        self.attention_title = AdditiveAttention(
            d_h=NUMBER_FILTER_CNN,
            hidden_size=QUERRY_DIM
        )

        self.attention_abstract = AdditiveAttention(
            d_h=NUMBER_FILTER_CNN,
            hidden_size=QUERRY_DIM
        )

        self.attention_all = AdditiveAttention(
            d_h=NUMBER_FILTER_CNN,
            hidden_size=QUERRY_DIM
        )

    def forward(self, x):
        """
            Encoder news
            x shape: batch_size*number_news*(1 + 1 + 30 + 50)
                1: category
                1: category
                30: title length
                50: abstract length
            return shape: bz*num*400
        """
        bz = x.shape[0]  # batch size
        num = x.shape[1]  # number of news
        _x = x.flatten(0, 1)  # flat*(1+1+30+50) #flat = bz * num

        category = _x[:, :1]  # flat*1
        subcategory = _x[:, 1:2]  # flat*1
        title = _x[:, 2:2 + TITLE_SIZE]  # flat*30
        abstract = _x[:, 2+TITLE_SIZE:]  # flat*50

        category_emb = self.category_embedding(category)  # flat*1*100
        category_linear = self.category_linear(category_emb)  # flat*1*400

        subcategory_emb = self.subcategory_embedding(subcategory)  # flat*1*100
        subcategory_linear = self.subcategory_linear(
            subcategory_emb
        )  # flat*1*400

        title_emb = self.word_embedding(title)
        title_drop = self.title_drop(title_emb)  # flat*30*300
        title_drop = title_drop.unsqueeze(1)  # flat*1*30*300
        title_cnn = self.title_cnn(title_drop)
        title_relu = self.title_relu(title_cnn)  # flat*400*30*1
        title_relu = title_relu.squeeze(-1).transpose(2, 1)  # flat*30*400
        title_att = self.attention_title(title_relu)  # flat * 400

        abstract_emb = self.word_embedding(abstract)
        abstract_drop = self.abstract_drop(abstract_emb)  # flat*50*300
        abstract_drop = abstract_drop.unsqueeze(1)  # flat*1*50*300
        abstract_cnn = self.abstract_cnn(abstract_drop)
        abstract_relu = self.abstract_relu(abstract_cnn)  # flat*400*50*1

        # flat*50*400
        abstract_relu = abstract_relu.squeeze(-1).transpose(2, 1)
        abstract_att = self.attention_abstract(abstract_relu)  # flat * 400

        concate = torch.concatenate([
            category_linear,
            subcategory_linear,
            title_att.unsqueeze(1),
            abstract_att.unsqueeze(1)
        ], dim=1)  # flat*4*400
        new_encode = self.attention_all(concate)  # flat*400
        new_encode = new_encode.reshape(
            bz,
            num,
            NUMBER_FILTER_CNN
        )  # bz * num * 400
        return new_encode


class UserEncoder(nn.Module):
    def __init__(self):
        super(UserEncoder, self).__init__()

        self.user_attention = AdditiveAttention(
            d_h=NUMBER_FILTER_CNN,
            hidden_size=QUERRY_DIM
        )

    def forward(self, user_present):
        out = self.user_attention(user_present)
        return out


# test model
if __name__ == "__main__":
    train = Data(mode="val")
    dataload = DataLoader(train, batch_size=32)
    it = iter(dataload)
    batch = next(it)
    model = NAMLBase()
    model(
        batch["history_category"],
        batch["history_subcategory"],
        batch["history_title"],
        batch["history_abstract"],
        batch["candidate_category"],
        batch["candidate_subcategory"],
        batch["candidate_title"],
        batch["candidate_abstract"],
    )
