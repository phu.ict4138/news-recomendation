BATCH_SIZE = 16
LEARNING_RATE = 0.0001
DROPOUT = 0.2
STEP_LOSS = 500
EPOCH = 3

NPRATIO = 4

TITLE_SIZE = 30
ABSTRACT_SIZE = 50
TITLE_ENTITY_SIZE = 5
ABSTRACT_ENTITY_SIZE = 10
HISTORY_SIZE = 50

NUMBER_CATEGORY = 1+17
NUMBER_SUBCATEGORY = 1+264
NUMBER_USER = 1 + 50000
NUMBER_WORD = 1 + 46741
NUMBER_ENTITY = 1 + 26902

CATEGORY_DIM = 100
SUBCATEGORY_DIM = 100

WORD_EMBEDDING_DIM = 300
ENTITY_EMBEDDING_DIM = 100
NUMBER_FILTER_CNN = 400
WINDOW_SIZE = 3

QUERRY_DIM = 200

DEVICE = "cuda:0"
